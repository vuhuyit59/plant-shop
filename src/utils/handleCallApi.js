import axios from 'axios';

export default function* callApi(
    method,
    options = {
        url: null,
        data: null,
        token: null,
        params: null,
        useLocalToken: false,
        callback: null,
        raiseError: true,
        useDefaultUrl: true,
    }
) {
    try {
        const newUrl = options.url;
        const useDefaultUrl =
            options.useDefaultUrl != null ? options.useDefaultUrl : true;
        const token = options.token;
        const params = options.params;
        const useLocalToken =
            options.useLocalToken != null ? options.useLocalToken : true;
        const data = options.data;
        // const url = useDefaultUrl ? `${constant.baseUrl}${newUrl}` : newUrl;
        const url = newUrl;
        let newToken = useLocalToken
            ? localStorage.getItem('token_user')
            : null;
        newToken = newToken ? newToken : token;
        const config = {
            headers: newToken
                ? {
                      Authorization: `Bearer ${newToken}`,
                  }
                : {},
            params,
        };
        let response = false;
        switch (method.toUpperCase()) {
            case 'GET':
                response = yield axios.get(url, config);
                break;
            case 'PUT':
                response = yield axios.put(url, data, config);
                break;
            case 'POST':
                response = yield axios.post(url, data, config);
                break;
            case 'DELETE':
                response = yield axios.delete(url, config);
                break;
            default:
                break;
        }
        return response?.data;
    } catch (apiError) {
        // const raiseError = options.raiseError != null ? options.raiseError : true;
        // const response = apiError?.response;
        // console.log('Call api error: ', response);
        // if (!raiseError) {
        //     return response;
        // }
        // let errorMessage = `Call api ${options.url} failed`;
        // if (response?.data?.error?.message)
        //     errorMessage = response.data.error.message;
        // yield put({type: appAction.RAISE_ERROR, payload: {error: errorMessage}});
        return false;
    }
}
