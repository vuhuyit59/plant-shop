import { Dimensions } from 'react-native';
import constant from '../constants';

const { width } = Dimensions.get('window');

const ratioW = (widthData) => {
    return widthData * (width / constant.withConstant);
};

export default {
    ratioW,
};
