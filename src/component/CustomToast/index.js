import React, { useState, useEffect } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import PropType from 'prop-types';
import Text from '../CustomText';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const CustomToast = (props) => {
    let mounted = true;
    const [showToast, setShowToast] = useState(false);
    const [toastDuration, setToastDuration] = useState(false);

    useEffect(() => {
        return () => {
            mounted = false;
        };
    }, []);

    const handleToastController = () => {
        if (props.open && mounted) {
            setShowToast(true);
            setToastDuration(true);
            setTimeout(
                () => {
                    if (!toastDuration) {
                        setShowToast(false);
                        setToastDuration(false);
                        if ('onChangeOpenStatus' in props)
                            props.onChangeOpenStatus(false);
                    }
                },
                'openDuration' in props && props.openDuration != null
                    ? props.openDuration
                    : 5000
            );
        }
    };

    useEffect(() => {
        handleToastController();
    }, []);

    useEffect(() => {
        if (mounted) handleToastController();
    }, [props.open]);

    return showToast && props.message ? (
        <Animatable.View
            style={styles.toastRelativeContainer}
            animation="slideInDown"
            iterationCount={1}
            direction="alternate"
        >
            <LinearGradient
                colors={
                    props.color
                        ? props.color
                        : ['#FF305C', '#FF4F34', '#FF7800']
                }
                start={{ x: 0, y: 0 }}
                end={{ x: 0, y: 1 }}
                style={styles.toastAbsoluteContainer}
            >
                <Text style={styles.toastMessage}>{props.message}</Text>
            </LinearGradient>
        </Animatable.View>
    ) : null;
};

const styles = StyleSheet.create({
    toastRelativeContainer: {
        width: screenWidth,
        zIndex: 10,
        justifyContent: 'center',
        alignItems: 'flex-end',
        display: 'flex',
        position: 'absolute',
        top: screenHeight * 0.15,
        right: 20,
    },
    toastAbsoluteContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 10,
        overflow: 'hidden',
        maxWidth: screenWidth * 0.6,
    },
    toastMessage: {
        color: '#fff',
        fontSize: 15,
    },
});

export default CustomToast;

CustomToast.propTypes = {
    color: PropType.array,
    open: PropType.bool,
    onChangeOpenStatus: PropType.func,
    openDuration: PropType.number,
    message: PropType.string,
};
