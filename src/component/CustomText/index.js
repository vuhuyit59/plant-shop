// @flow
import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

export default class TextCustom extends Component {
    render() {
        return (
            <Text
                {...this.props}
                style={[styles.defaultStyle, this.props?.style]}
            >
                {this.props.children}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    defaultStyle: {
        fontFamily: 'HiraginoSans-W6',
        fontWeight: 'normal',
    },
});

TextCustom.propTypes = {
    style: PropTypes.any,
    children: PropTypes.any,
};
