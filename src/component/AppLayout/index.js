import React from 'react';
import { SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';

const Layout = (props) => {
    return <SafeAreaView>{props.children}</SafeAreaView>;
};

export default Layout;

Layout.propTypes = {
    navigation: PropTypes.object,
    children: PropTypes.object,
};
