import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

export const useColor = (generateStyle = () => {}) => {
    const baseColor = useSelector((state) => state.commonReducers.baseColor);
    return {
        styles: generateStyle(baseColor),
        baseColor,
    };
};

export default useColor;

useColor.propTypes = {
    generateStyle: PropTypes.func,
};
