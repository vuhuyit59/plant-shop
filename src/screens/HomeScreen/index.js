import React, { useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import AuthAction from '../../stores/actions/authAction';
import PropTypes from 'prop-types';
import utils from '../../utils';

const HomeScreen = (props) => {
    const [example, setExample] = useState(null);
    let mounted = true;

    useEffect(() => {
        return () => {
            mounted = false;
        };
    });

    const setState = (payload, stateFunction) => {
        if (mounted) {
            stateFunction(payload);
        }
    };

    const handleClickBtn = () => {
        setState('test', setExample);
        props.onTest();
    };

    return (
        <View style={styles.homeContainer}>
            <Text style={styles.homeText}>This is home screen</Text>
            <Button onPress={handleClickBtn} title="OK" />
        </View>
    );
};

const styles = StyleSheet.create({
    homeContainer: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    homeText: {
        color: 'red',
        marginBottom: 50,
        fontSize: utils.ratioW(14),
    },
});

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
    onTest: AuthAction.onTest,
})(HomeScreen);

HomeScreen.propTypes = {
    onTest: PropTypes.func,
};
