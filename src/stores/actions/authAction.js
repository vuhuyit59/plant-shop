import authConstant from '../constants/authConstant';

const onTest = () => ({
    type: authConstant.ON_LOGOUT_DONE,
    payload: 'red',
});

export default {
    onTest,
};
