import authConstant from '../../constants/authConstant';

const commonReducers = (state = { loading: false }, action) => {
    if (action.type === authConstant) {
        return {
            loading: true,
        };
    }
    return state;
};

export default commonReducers;
