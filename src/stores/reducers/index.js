import { combineReducers } from 'redux';
import authConstant from '../constants/authConstant';
import commonReducers from './commonReducer';

const allReducers = combineReducers({
    commonReducers,
});

const returnReducers = (state, action) => {
    if (action.type === authConstant.ON_LOGOUT_DONE) {
        state = undefined;
    }
    if (action.type === authConstant.ON_SUBMIT_LOGIN) {
        state = {
            userReducers: state ? state.userReducers : null,
        };
    }
    return allReducers(state, action);
};

export default returnReducers;
