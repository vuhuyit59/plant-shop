import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, createStore } from 'redux';
import allReducers from './reducers';
import rootSaga from './sagas';
import loggerMiddleware from './middleware/loggerMiddleware';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

const sagaMiddleware = createSagaMiddleware();
// create the persist store
const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['auth', 'middleware', 'readCache'],
    // blacklist: ['partnerDetails', 'options', 'userProfile', 'search', 'footprint', 'badges'],
    timeout: null,
};
const reducer = persistReducer(persistConfig, allReducers);
const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware, loggerMiddleware)
);
const persist = persistStore(store);
sagaMiddleware.run(rootSaga);

export { store, persist };
