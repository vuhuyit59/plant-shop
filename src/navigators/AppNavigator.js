import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeStackNavigator from './HomeStackNavigator';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
    return (
        <NavigationContainer>
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: (data) => {
                        const { focused, color, size } = data;
                        let iconName;
                        if (route.name === 'homeStack') {
                            iconName = 'ios-home';
                        } else if (route.name === 'homeStack2') {
                            iconName = focused ? 'ios-list-box' : 'ios-list';
                        }
                        return (
                            <Ionicons
                                name={iconName}
                                size={size}
                                color={color}
                            />
                        );
                    },
                })}
                tabBarOptions={{
                    activeTintColor: 'blue',
                    inactiveTintColor: 'gray',
                }}
            >
                <Tab.Screen
                    name="homeStack"
                    component={HomeStackNavigator}
                    options={{}}
                />
                <Tab.Screen
                    name="homeStack2"
                    component={HomeStackNavigator}
                    options={{}}
                />
            </Tab.Navigator>
        </NavigationContainer>
    );
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(BottomTabNavigator);
