module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "node": true
    },
    "extends": [
        "plugin:react/recommended",
        "google",
        "eslint:recommended",
        "prettier",
        "prettier/flowtype",
        "prettier/react",
        "prettier/standard"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "prettier"
    ],
    "rules": {
        "react/display-name" : "off",
        "no-unused-vars" : "warn",
        "react/prop-types" : "warn",
        "require-jsdoc": "off"
    }
};
