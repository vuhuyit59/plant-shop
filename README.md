## Available Scripts

In the project directory, you can run:

### `yarn install`

then

### `yarn run android`

Runs the app in the android device.<br>

### `yarn run ios`

Runs the app in the ios device.<br>

### `yarn run ios:device {udid}`

Runs the app in the ios device through the specific udid.<br>
You can get the ios simulator device uuid with this command : `instruments -s devices`

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


### `yarn run web`

Runs the app in the web browser.<br>
See the section about [running on web](https://docs.expo.io/guides/running-in-the-browser/) for more information.


### `yarn run lint`

Check coding convention and check error with Eslint.<br>
See the section about [Eslint](https://eslint.org/docs/user-guide/getting-started) for more information.

### `yarn run lint:fix`

Fix coding convention with Eslint.<br>
See the section about [Eslint](https://eslint.org/docs/user-guide/getting-started) for more information.


### `yarn run format`

Make code prettier with Prettier.<br>
See the section about [Prettier](https://prettier.io/docs/en/index.html) for more information.


### `yarn run precommit`

Check everything before commit.<br>

## Commit message 

In the project, we use commit lint to restrict to do all commit messages in a proper manner.<br>
See more about commit convention [Commitlint](https://commitlint.js.org/#/)
